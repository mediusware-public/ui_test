<nav class="navbar fixed-top navbar-expand-lg  bg-transparent bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">FAQ</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="#">Rules</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="#">About</a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item active">
                <button class="btn btn-secondary mr-2" data-toggle="modal" data-target="#loginModal">LOGIN</button>
            </li>
            <li class="nav-item active">
                <button class="btn btn-primary">JOIN NOW</button>
            </li>
        </ul>
    </div>
</nav>


<div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="loginModalLabel">WELCOME BACK <span
                        class="d-block">Sign in to Clique Picks</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <login-component></login-component>
            </div>
            <center class="modal-footer d-block">
                <p>or sign in with</p>
                <div class="row">
                    <div class="col">
                        <button class="btn btn-primary btn-block"><i class="fab fa-facebook-f"></i></button>
                    </div>
                    <div class="col">
                        <button class="btn btn-light btn-block"><i class="fab fa-google"></i></button>
                    </div>
                </div>
                <p class="mt-3">Don't have an account ? <a href="">Sign Up</a></p>
            </center>
        </div>
    </div>
</div>
